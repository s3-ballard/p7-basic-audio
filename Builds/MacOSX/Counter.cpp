//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 03/11/2016.
//
//

#include "Counter.hpp"


Counter::Counter() : Thread ("CounterThread")
{
    startThread();
    counter = 0;
    countTime = 200;
    listener = nullptr;
    
}

Counter::~Counter()
{
    stopThread(500);
}


void Counter::run()
{
    
    while (!threadShouldExit())
    {
        //uint32 time = Time::getMillisecondCounter();
        //std::cout << "Counter:" << Time::getApproximateMillisecondCounter() << "\n";
        //Time::waitForMillisecondCounter(time + 100);
        uint32 time = Time::getMillisecondCounter();
        if (listener != nullptr)
            listener->counterChanged (counter++);
        Time::waitForMillisecondCounter(time + (60000 / countTime.get()));
        //std::cout << "Counter:" << counter << "\n";
        
        
    }
}
//add audio as inhereted and then trigger beep during run

void Counter::stopOrStartCounter()
{
    if (isThreadRunning() == true)
    {
        stopThread(500);
        counter = 0;
    }
    
    else if (isThreadRunning() == false)
    {
        startThread();
    }
    
}

void Counter::setListener(Listener* newListener)
{
    listener = newListener;

}
