//
//  Counter.hpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 03/11/2016.
//
//

#ifndef Counter_hpp
#define Counter_hpp


#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

class Counter:public Component, public Thread

{
    
   
    
public:
    
    Counter();
    ~Counter();
    
    void run() override;
    
    void stopOrStartCounter();
    
    
    class Listener
    {
    public:
        virtual ~Listener() {}
        
        virtual void counterChanged (const unsigned int counterValue) = 0;
        
    private:
    };
    

    Atomic <uint32> countTime;
    void setListener (Listener* newListener);
private:
    int counter;
 
    Listener *listener;
    
};




#endif /* Counter_hpp */
