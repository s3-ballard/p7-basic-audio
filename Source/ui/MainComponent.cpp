/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) :  audio (a)
{

    setSize (500, 400);
    addAndMakeVisible(button1);
    ampSlider.setSliderStyle(Slider::LinearHorizontal);
    ampSlider.setRange(0, 1);
    button1.setButtonText("press to stop / start");
    button1.setBounds(10, 90, getWidth()-20, 40);
    button1.addListener(this);
    
    addAndMakeVisible(tempoSlider);
    tempoSlider.setSliderStyle(Slider::LinearHorizontal);
    tempoSlider.setRange(20, 400);
    tempoSlider.setBounds(10, 150, getWidth()-20, 40);
    tempoSlider.addListener(this);
    
    addAndMakeVisible(count);
    count.setListener(this);
    //count.run();
    

}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    
}


//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    
    if (slider == &ampSlider)
    {
       // audio.amp = ampSlider.getValue();
    }
    
    if (slider == &tempoSlider)
    {
        count.countTime = tempoSlider.getValue();
    }
    
}

void MainComponent::buttonClicked(Button* button)
{
    count.stopOrStartCounter();
    
}

void MainComponent::counterChanged(const unsigned int counterValue)
{
    std::cout << "Counter:" << counterValue << "\n";
    audio.Beep();
    
}
